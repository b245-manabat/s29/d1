// Mini Activity

        // Re-insert the record of Bill Gates in the users database.
        /*

            firstName - "Bill"
            lastName - "Gates"
            age: 65
            contact: {
                phone - "12345678",
                email: "bill@gmail.com"
            },
            courses -  "PHP", "Laravel", "HTML"
            department: "Operations",

        */
        // Take a screenshot of result and send it to the batch hangouts.

        db.users.insertOne ({
            firstName: "Bill",
            lastName: "Gates",
            age: 65,
            contact: {
                phone: "12345678",
                email: "bill@gmail.com"
            },
            courses:  ["PHP", "Laravel", "HTML"],
            department: "Operations"
        })

// [SECTION] Comparison Query Operators
    // $gt/$gte operator
        /*
            - allows us to find documents that have field values greater than or equal to specified value.
            - Syntax:
                db.collectionName.find({field: {$gt: value}});
                db.collectionName.find({field: {$gte: value}});
        */

        db.users.find({age:{$gt:65}}); // will return 2 records
        db.users.find({age:{$gte:65}}); // will return 3 records, including age equal to 65

    // $lt/$lte operator
        /*
            - allows us to find documents that have field values lesser than or equal to specified value.
            - Syntax:
                db.collectionName.find({field: {$lt: value}});
                db.collectionName.find({field: {$lte: value}});
        */

        db.users.find({age:{$lt:65}}); // will return 2 records
        db.users.find({age:{$lte:65}}); // will return 3 records, including age equal to 65

    // $ne operator
        /*
            - Allows us to find document that have field number values not equal to a specified value.
            - Syntax:
                db.collectionName.find({field: {$ne: value}}),
        */

        db.users.find({age: {$ne:82}});

    // $in operator
        /*
            - allows us to find documents with specific match criteria of one field using different values.
            - Syntax:
                db.collectionName.find({field: {$in:[valueA,valueB, . . .]}})
        */

        db.users.find({lastName: {#in:["Hawking", "Doe"]}});
        db.users.find({courses: {#in:["HTML", "React"]}});

// [SECTION] 
    // #or operator
        /*
            - allows us to find documents that match a single criteria from multiple provided search criteria.
            - Syntax:
                db.collectionName.find({$or:[{fieldA:valueA},{fieldB:valueB}]})
        */

        db.users.find({
            $or:[
                    {firstName:"Neil"},
                    {age:25}
                ]
        });

        // with comparison query operator

        db.users.find({
            $or:[
                    {firstName:"Neil"},
                    {age:{$gt:30}}
                ]
        });

    // $and
        /*
            - allows us to fund documents matching multiple criteria in a single field.
            - Syntax:
                db.collectionName.find({$and:[{fieldA:valueA},{fieldB:valueB}]})
        */

        db.users.find({
            $and: [
                    {age: {$ne:82}},
                    {age: {$ne:76}}
                ]
        });

// Mini Activity

        // Look for the users that have the courses "Laravel" & "React" and whose age is less than 80 years old.

        // Take a screenshot of result and send it to the batch hangouts.

        // Expected Result: Stephen Hawking and Bill Gates

        db.users.find({
              $and: [
                {courses: {$in: ["Laravel","React"]}},
                {age: {$lt:80}}
                ]
            });


// [SECTION] Field Projection
    // to help with readability of the values returned, we can include/exclude fields from the retrieved results.

    // Inclusion
        /*
            - allows us to include/add specific fields only when retrieving documents.
            - the value provided is 1 to denote that the field being included.
            - Syntax:
                db.collectionName.find({criteria}, {field:1});
        */

        db.users.find(
            {
                firstName: "Jane"
            },
            {
                firstName: 1,
                lastName: 1,
                contact: 1
            }
        );

    // Exclusion
        /*
            - allows us to exclude/remove specific fields only when retrieving documents.
            - the value provided is 0 to denote that the field being excluded.
            - Syntax:
                db.collectionName.find({criteria}, {field:0});
        */

        db.users.find(
            {
                firstName: "Jane"
            },
            {
                _id:0,
                contact:0,
                department: 0
            }
        );

// Mini Activity
        
        // Using the Field projection, Return the User's firstName, lastName, and contact field where lastName is equal to "Doe".

        // Take a screenshot of result and send it to the batch hangouts.

        db.users.find(
            {
                lastName: "Doe"
            },
            {
                _id:0,
                firstName:1,
                lastName: 1,
                contact: 1
            }
        );


    // suppresing the _id field
        // - when using field projection, field inclusion and exclusion may not be used at the same time.
        // - excluding the "_id" field is the only exception to this rule.
        // - Syntax:                db.collectionName.find({criteria}, {field:1, _id:0});


    // return a specific field in embedded documents.
        db.users.find(
                {
                    firstName: "Jane"
                },
                {
                    firstName:1,
                    lastName:1,
                    "contact.phone":1
                }
            )

    // project specific elemetns in the returned array.
        // The $slice operator allows us to retrieve element that matches a criteria.
        // Syntax:
            // db.collectionName.find({criteria}, {arrayField:{$slice: count}});
            // db.collectionName.find({criteria}, {arrayField:{$slice: [index, count]}});

        // show the elements array based on slice count.
        db.users.find(
                {
                    firstName:"Jane"
                },
                {
                    courses: {$slice:2}
                }
            )

        // 
        db.users.find(
                {
                    firstName:"Jane"
                },
                {
                    courses: {$slice:[1,1]}
                }
            )

// [SECTION] Evaluation Query Operator
    
        db.users.find({firstName:"jane"}); // will not run, case sensitive

    // $regex operator
        /*
            - allows us to find documents that much a specific string pattern using "regular expression"/"regex".
            - Syntax:
                db.collectionName.find({field: $regex "pattern"});
        */

        // case sensitive query
        db.users.find({firstName: {$regex: "Ne"}});

        // case insensitive query
        db.users.find({firstName: {$regex: "Ne", $options:"$i"}});

        /*
            other options for reference:
                https://www.mongodb.com/docs/manual/reference/operator/query/regex/
        */